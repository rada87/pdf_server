var express = require('express');
var bodyParser = require('body-parser');
var puppeteer = require('puppeteer');

/**
 * Init
 */

var app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies



/**
 * Routes
 */

app.post('/pdf/render', function (req, res) {
    var content = req.body.content;

    printPDF(content).then(result => {
        res.send( result );
    }).catch(error => {
        console.log(error)
    });
});



/**
 * Start server
 */
var port = process.env.PORT || 666;
app.listen(port);
console.log('Server started at port ' + port);


/**
 * Functions
 */

async function printPDF(html) {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    await page.setContent(html);
    var result = await page.pdf({
        printBackground: true,
        margin: {
            top: '50px'
        },
        format: 'A4'
    });

    await browser.close();
    return result;
}



